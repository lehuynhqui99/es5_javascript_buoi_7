let mangSoNguyen = []

const number = document.querySelector("#number");
const showMangSoNguyen = document.querySelector(".showMangSoNguyen");

const soBatKy = document.querySelector("#soBatKy");



let tongSoDuong = 0
let soDuong = 0

document.querySelector(".nhapBtn").addEventListener('click', e=> {
    e.preventDefault()


    // validation
    if (!number.value) {
        number.value = 0
    }

    if (number.value.includes('.') || number.value.includes(',')) {
        alert("Trường nhập vào phải là số nguyên")
        return;
    }

    // Đẩy giá trị người dùng nhập vào mãng
    mangSoNguyen.push(number.value)
    showMangSoNguyen.innerHTML = mangSoNguyen


    // cho phép nhấn nút thực hiện các phép toán
    if (mangSoNguyen.length !== 0) {
        document.querySelector(".thucHienBtn").disabled = false
    }

    document.querySelector(".thucHienBtn").addEventListener('click', e => {
        // khoá tính năng nhập vào giá trị mới
        document.querySelector(".nhapBtn").disabled = true

        // Mở khoá tính năng thực hiện các phép toán
        document.querySelectorAll(".phepTinh button").forEach(button => {
            button.disabled = false
        })
    })
})



// bài 1
document.querySelector(".tongDuongBtn").addEventListener('click', e => {
    mangSoNguyen.forEach(so => {
        if ( Number(so) > 0) {
            tongSoDuong += Number(so)
        } 
    })
    document.querySelector(".showTongCacSoDuong").innerHTML= tongSoDuong
})


//bài 2
document.querySelector(".coBaoNhieuSoDuongBtn").addEventListener('click', e => {
    mangSoNguyen.forEach(so => {
        if ( Number(so) > 0) {
            soDuong++
        } 
    })
    document.querySelector(".showBaoNhieuSoDuong").innerHTML= soDuong
})


// bài 3
document.querySelector(".soNhoNhatBtn").addEventListener('click', e => {
   let soNhoNhat = Number(mangSoNguyen[0])
    for (let i = 1; i < mangSoNguyen.length; ++i) {
        if (soNhoNhat > mangSoNguyen[i]) {
            soNhoNhat = Number(mangSoNguyen[i])
        }
    }
    document.querySelector(".showSoNhoNhat").innerHTML= soNhoNhat
})


// bài 4
document.querySelector(".soDuongNhoNhatBtn").addEventListener('click', e => {
    let mangSoDuong = []
    for (let i = 0; i < mangSoNguyen.length; ++i) {
        if (Number(mangSoNguyen[i]) > 0) {
            mangSoDuong.push(mangSoNguyen[i])
        }
    }

    let soDuongNhoNhat = Number(mangSoDuong[0])

    if (mangSoDuong.length !== 0) {

        for (let i = 0; i < mangSoDuong.length; ++i) {
            if (soDuongNhoNhat > mangSoDuong[i]) {
                soDuongNhoNhat = Number(mangSoDuong[i])
            }
        }
    } else {
        soDuongNhoNhat = `Không có số dương, nên không tìm được số dương nhỏ nhất`
    }

    document.querySelector(".showSoDuongNhoNhat").innerHTML= soDuongNhoNhat
})
 


// bài 5
document.querySelector(".soChanCuoiCungBtn").addEventListener('click', e => {
    let soChanCuoiCung
     for (let i = mangSoNguyen.length; i >= 0; --i) {
        if (Number(mangSoNguyen[i]) %2 ==0) {
        soChanCuoiCung = Number(mangSoNguyen[i])
        break;
        } else {
            soChanCuoiCung = -1
        }
     }
     document.querySelector(".showSoChanCuoiCung").innerHTML= soChanCuoiCung
})






// bài 6
document.querySelector(".doiChoBtn").addEventListener('click', e => {
    const viTri1 = document.querySelector("#viTri1");
    const viTri2 = document.querySelector("#viTri2");

    if(!viTri1.value || !viTri2.value 
        || Number(viTri1.value) <= 0 || Number(viTri2.value) <= 0 
        || Number(viTri1.value) ===  Number(viTri2.value)
        || viTri1.value.includes(".") || viTri1.value.includes(",")
        || viTri2.value.includes(".") || viTri2.value.includes(",") 
        || Number(viTri1.value) > Number(mangSoNguyen.length) || Number(viTri2.value) > Number(mangSoNguyen.length)) {

            alert ('Các trường không được bỏ trống. Giá trị nhập vào phải là số nguyên dương; không được bằng nhau và không vượt quá độ dài của mãng.')
            return;
        }
  
        let soTamThoi = mangSoNguyen[Number(viTri1.value - 1)]

        mangSoNguyen.splice(Number(viTri1.value - 1), 1, mangSoNguyen[Number(viTri2.value - 1)])
        
        mangSoNguyen.splice(Number(viTri2.value - 1), 1, soTamThoi)

        viTri1.value = ''
        viTri2.value = ''
    
     document.querySelector(".showMangMoi").innerHTML= mangSoNguyen
})


// bài 7
document.querySelector(".tangDanBtn").addEventListener('click', e => {

    // mangSoNguyen.sort(function(a,b){
    //     return a - b
    // })
    //  document.querySelector(".showTangDan").innerHTML= mangSoNguyen


    for (let i = 0 ; i < mangSoNguyen.length-1; ++i) {
        for (let j = i + 1 ; j < mangSoNguyen.length; ++j) {
            if (Number(mangSoNguyen[i]) > Number(mangSoNguyen[j])) {
                let temp = mangSoNguyen[i];
                mangSoNguyen[i] = mangSoNguyen [j];
                mangSoNguyen [j] = temp
            }
        }
    }

    document.querySelector(".showTangDan").innerHTML= mangSoNguyen
})

// bài 8
document.querySelector(".soNguyenToBtn").addEventListener('click', e => {
    let soNguyenTo

    for (let i = 0 ; i < mangSoNguyen.length; ++i) {
        if (Number(mangSoNguyen[i]) === 2 || Number(mangSoNguyen[i]) === 3 || Number(mangSoNguyen[i]) === 5 || Number(mangSoNguyen[i]) === 7){
            soNguyenTo = mangSoNguyen[i]
            break;
        }

        if (Number(mangSoNguyen[i]) > 10 && Number(mangSoNguyen[i])%2 !==0 && Number(mangSoNguyen[i])%3 !==0 && Number(mangSoNguyen[i])%4 !==0 && Number(mangSoNguyen[i])%5 !==0 
            && Number(mangSoNguyen[i])%6 !==0 && Number(mangSoNguyen[i])%7 !==0 && Number(mangSoNguyen[i])%8 !==0 && Number(mangSoNguyen[i])%9 !==0 && Number(mangSoNguyen[i])%10 !==0) {
            soNguyenTo = mangSoNguyen[i]
            break;
        }

        soNguyenTo = -1
    }
    
    document.querySelector(".showSoNguyenTo").innerHTML= soNguyenTo
})

// bài 9
document.querySelector(".soNguyenBtn").addEventListener('click', e => {
    if (!soBatKy.value) {
        alert ('Trường này là bắt buộc.')
        return;
    }

    mangSoNguyen.push(soBatKy.value)

    let soNguyen = 0
    mangSoNguyen.forEach(so => {
        if(so%1 ===0) {
            soNguyen ++
        }
    })
     document.querySelector(".showSoNguyen").innerHTML= soNguyen + `<h3 class="alert-danger">Mãng mới là: ${mangSoNguyen} </h3>`
})



// bài 10
document.querySelector(".soSanhBtn").addEventListener('click', e => {
    let duong = 0;
    let am = 0;


    for (let i = 0; i < mangSoNguyen.length; ++i) {
        if(Number(mangSoNguyen[i]) > 0) {
            duong++
        } 

        if(Number(mangSoNguyen[i]) < 0) {
            am++
        }
    }

    console.log(duong, am)
    if (duong > am) {
        document.querySelector(".showSoSanh").innerHTML = 'Số dương nhiều hơn số âm'
        return;
    }

    if (duong < am) {
        document.querySelector(".showSoSanh").innerHTML = 'Số dương ít hơn số âm'
        return;

    }

    if (duong = am) {
        document.querySelector(".showSoSanh").innerHTML =  'Số dương và số âm bằng nhau'
        return;

    }

})

